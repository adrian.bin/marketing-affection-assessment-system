import xlrd, webbrowser, urllib
from itertools import takewhile


def column_len(sheet, index):
    col_values = sheet.col_values(index)
    col_len = len(col_values)
    for _ in takewhile(lambda x: not x, reversed(col_values)):
        col_len -= 1
    return col_len


def date(start_date, end_date):
    # FORMAT URL DECODED
    # cdr:1,cd_min:7/2/2019,cd_max:7/9/2019
    # cdr --> How many date
    # cd_min, cd_max --> Minimum date, Maximum date

    date_string = "cdr:1,cd_min:" + str(start_date) + ",cd_max:" + str(end_date)
    return get_encoded_value(date_string)


# Not Done Yet
def get_encoded_value(data):
    return urllib.quote_plus(data)


def return_site(site):
    return site


def get_excel_content(worksheet):
    # print(worksheet.col_values(0))
    return worksheet.col_values(0)


def convert_excel_content(items):
    result = []
    site_param = "site:"
    for item in items:
        item = site_param+item
        item = get_encoded_value(item)

        result.append(item)
    return result


def convert_query(query):
    search = "https://www.google.com/search?q="
    search_string = search + get_encoded_value(query)
    return search_string


# Hamburger theory
# Getting initial google query      (Top bread)
user_query = raw_input("Please Enter the query: ")
init_query = convert_query(user_query)


# Search by site                    (Meat in the Middle)
workbook = xlrd.open_workbook('Book1.xlsx')
worksheet = workbook.sheet_by_index(0)
raw_site = get_excel_content(worksheet)                                      
cooked_site = convert_excel_content(raw_site)


# Search by time frame              (Button Bread)
user_date_start = raw_input("Please Enter Start Time(Format MM/DD/YYYY):  ")
user_date_end = raw_input("Please Enter End Time(Format MM/DD/YYYY):  ")
query_date = "tbs=" + date(user_date_start, user_date_end)


# Stack to serve
first = True
for site in cooked_site:
    this_query = init_query + "+" + site + "&" + query_date

    if(first):
        webbrowser.get('firefox').open_new(this_query)
        first = False 
    else:
        webbrowser.get('firefox').open_new_tab(this_query)





